# Git Automatic Release BrAnch GenErator

This creates git release branches, usually based on the last branch merged. 
Since this is gitlab, there's a certain way to the logging there. 

## Prerequisites

well, you'll need:

- python3
- git

but i'm pretty sure they already come in a normal debian install

## Installation

~~~
git clone https://gitlab.com/joesiw/git-auto-release
cd git-auto-release
# tested in debian 10 and centos 7 only.
sudo make install 
~~~

## Usage

### At the start

First. create a release with: 

~~~
## create repo on gitlab.com
git clone git@gitlab.com/group/repo.git
cd repo
echo "sdckmsd" > somefile
git add .
git commit -a -m "make sure there's a commit there"
##
git nextrelease v0.0.1
~~~

### After a branch merge 

on the maintainers machine

~~~
git aftermerge (patch,major,minor)
~~~


### pre_tag.sh

so ... when you're playing with poetry and other automation/v.e. type things... it's fun to tie it
into a pre_tag.sh

### post_tag.sh

well ... this seems like it's more useful to be honest.

### new_release.sh

one of these gd entrypoints will be useful...
