ifneq (,$(wildcard /etc/redhat-release))
    USRLIB := /usr/libexec/git-core
else
    USRLIB := /usr/lib/git-core
endif
CWD=$(shell pwd)

install:
ifneq ($(shell id -u),0)
		@echo "you need to run this as root"
else
		@test -f $(USRLIB)/git-aftermerge || ln -s $(CWD)/git-aftermerge $(USRLIB)/git-aftermerge
		@echo "git-aftermerge installed"
		@test -f $(USRLIB)/git-nextrelease || ln -s $(CWD)/git-nextrelease $(USRLIB)/git-nextrelease
		@echo "git-nextrelease installed"
endif

uninstall:
ifneq ($(shell id -u),0)
		@echo "you need to run this as root"
else
		@test -f $(USRLIB)/git-aftermerge && rm -f $(USRLIB)/git-aftermerge
		@echo "git-aftermerge uninstalled"
		@test -f $(USRLIB)/git-nextrelease && rm -f $(USRLIB)/git-nextrelease
		@echo "git-nextrelease uninstalled"
endif

